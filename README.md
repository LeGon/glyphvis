## Table of Contents

-   [About](#glyphvis)
-   [Setup](#setup)
-   [How to use](#how-to-use)
-   [Settings](#settings)

![preview](img/preview.png)

# GlyphVis 
###### Prototype  v1.3
GlyphVis can be used to display multivariate data as 3D glyphs.

Data input for this prototype is limited to enclosed data sets. These are data from this [source](https://www.kaggle.com/ruchi798/movies-on-netflix-prime-video-hulu-and-disney).
The row header of the CSV file was slightly changed to prevent problems during import. Furthermore, the number of data sets was reduced to make the test runs more performant.

#### Used tools, libraries and assets:

- Unity 2019.3.10f
- [CSV Serialize](https://assetstore.unity.com/packages/tools/integration/csv-serialize-135763)
- [Starfiel Skybox](https://assetstore.unity.com/packages/2d/textures-materials/sky/starfield-skybox-92717)
- [probuilder](https://unity3d.com/de/unity/features/worldbuilding/probuilder)

## Setup

1. Clone the repo
2. Open the project in Unity
3. File > Build And Run
4. Close the application
5. Copy the "data" folder and paste it to C:\Users\\*Username\*\AppData\LocalLow\DefaultCompany\GlyphVis\\.
(*the folder is created automatically when GlyphVis.exe is executed for the first time*)
6. Run GlyphVis.exe

## How to use

- Click on the button left to "Generate Glyphs" to show the selection panel for the data sets. After the selection has been changed, the "Generate Glyphs" button can be used to create the glyphs.
![loadData](img/loadData.png)

- Hover over glyph object to display its details. \
![hover](img/hover.png)

- Click on a glyph to mark it.
- Double click to jump to a glyph.
- Reset the camera origin with the arrow button next to the legend. ![legend](img/legend.png)
- Click and drag to rotate the camera.
- Scroll to zoom.

## Settings
#### Change View
You can change the arrangement of the glyphs by selecting the view.

- Random : *random positioning around a center point.*
- Kugel : *Positioning around a center point. The distance to the center depends on the score value of the glyph.*
- Solar : *Grouping of glyphs by score value. The further away from the center, the lower the score value.*


 ![views](img/views.png)

#### Change Shader
Change the shader to better identify the color values or to distinguish individual glyphs more clearly.

- Sprite
- Emission
- Outline


 ![shader](img/shader.png)

####  Change Background
You can change the background to create a better contrast or for better orientation.

Click "Apply" after changes have been made to the settings to commit them.

#### Enable/disable Billboard
Aligns the ring of the glyph to the camera so that the correct angle is always visible (see legend).
You can toggle the option without using the "Apply" button.