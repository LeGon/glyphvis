﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] public Transform target;
    public float radius = 20f;
    public float scrollscale = 0.5f;

    private Vector3 previousPosition;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            previousPosition = cam.ScreenToViewportPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 direction = previousPosition - cam.ScreenToViewportPoint(Input.mousePosition);

            cam.transform.position = target.position;

            cam.transform.Rotate(new Vector3(1, 0, 0), direction.y * 180);
            cam.transform.Rotate(new Vector3(0, 1, 0), -direction.x * 180, Space.World);
            cam.transform.Translate(new Vector3(0, 0, -radius));

            previousPosition = cam.ScreenToViewportPoint(Input.mousePosition);
        }

        if (Input.mouseScrollDelta.y != 0f)
        {
            radius += Input.mouseScrollDelta.y * -scrollscale;

            cam.transform.position = target.position;
            cam.transform.Translate(new Vector3(0, 0, -radius));

            previousPosition = cam.ScreenToViewportPoint(Input.mousePosition);
        }
    }
}
