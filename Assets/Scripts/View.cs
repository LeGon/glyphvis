﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class View
{
    static int nextId = 0;
    
    int vid;
    string vtitle;
    Vector3 vPos;
    public GameObject containerGO;
    public float maxRadius;
    List<View> viewList;
    Dictionary<int, Vector3> glyphPos;
    Dictionary<int, Quaternion> glyphOrient;
    List<GameObject> rings;


    public View(string title = "")
    {
        this.vid = Interlocked.Increment(ref nextId);
        this.vtitle = title;
    }

    public List<View> getViewList()    {return this.viewList;}

    public Dictionary<int, Vector3> getGlyphPos()   {
        if (this.glyphPos == null)
        {
            var meh = new Dictionary<int, Vector3>();
            meh.Add(-1, Vector3.zero);
            return meh;
        }

        return this.glyphPos;
    }

    public void AddView(View view)
    {
        if (this.viewList == null)
            this.viewList = new List<View>();
        viewList.Add(view);
    }

    public void setPosition(Vector3 pos)
    {
        this.containerGO = new GameObject(vtitle);
        this.containerGO.transform.position = pos;
        this.containerGO.AddComponent<MouseOverContainer>();
        this.vPos = pos;
    }

    public void solar(int distance)
    {
        rings = new List<GameObject>();
        Debug.Log("--> set Solar view of " + vtitle);
        int i = 0;
        foreach(View view in viewList)
        {
            float radius = distance * (viewList.Count - i++) - distance;
            Debug.Log("distance: " + distance +
                "\n" + "radius: " + radius +
                "\n" + "viewList.capacity: " + viewList.Count +
                "\n" + "viewList.Capacity - i: " + (viewList.Count - i - 1));
            float x = Random.Range(-radius, radius);
            float z = Mathf.Sqrt((radius * radius) - (x * x)) * Mathf.Pow(-1f,(i%2));
            float y = 1f;

            //Debug.Log("Erstelle GO");
            GameObject ringGO = new GameObject(vtitle.ToString() + "_ring_" + i);
            ringGO.transform.position = this.vPos;
            //Debug.Log("füge Circle hinzu");
            ringGO.AddComponent<Circle>();
            ringGO.GetComponent<Circle>().radius = radius;
            //Debug.Log("draw Circle");
            ringGO.transform.SetParent(this.containerGO.transform);
            ringGO.GetComponent<Circle>().drawCircle();
            rings.Add(ringGO);

            if (i == 0)
            {
                y = 0f;
                x = 0f;
                z = (float)distance;
            }
            view.setPosition(new Vector3(x, y, z));
            if (radius > maxRadius)
                maxRadius = radius;
        }
        this.containerGO.GetComponent<SphereCollider>().radius = this.maxRadius;
        Debug.Log("<-- set Solar view of " + vtitle);
    }

    public void planet(Dictionary<int, float> mass)
    {
        if (this.glyphPos == null)
            this.glyphPos = new Dictionary<int, Vector3>();
        
        Debug.Log("-->set Planet view of " + vtitle);
        if (vPos == null)
        {
            Debug.Log("keine Ursprungskoordinate in der View vorhanden");
            return;
        }

        float distance;

        foreach (KeyValuePair<int, float> item in mass)
        {
            
            if (item.Value == -1f)
                distance = 5f;
            else
                distance = 10 * (1-(item.Value - (int)item.Value));
            Vector3 random = new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), Random.Range(-5, 5));
            Vector3 newpos = vPos + (random.normalized * distance);
            this.glyphPos.Add(item.Key, newpos);
            if (distance > maxRadius)
                this.maxRadius = distance;
        }
        this.containerGO.GetComponent<SphereCollider>().radius = this.maxRadius;
        Debug.Log("<--set Planet view of " + vtitle);
    }

    public string toString()
    {
        string text = "";
        text = "vid: " + this.vid + "\n" +
            "vtitle: " + this.vtitle + "\n";
        if (vPos != null)
            text += "vpos: " + this.vPos + "\n" +
            "maxRadius: " + this.maxRadius + "\n";
        if (viewList != null && viewList.Count > 0)
            text += "viewList.count: " + this.viewList.Count + "\n";
        if (glyphPos != null && glyphPos.Count > 0)
            text += "glyphPos.count: " + this.glyphPos.Count + "\n";
        return text;
    }

}
