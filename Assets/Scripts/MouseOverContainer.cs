﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SphereCollider))]
public class MouseOverContainer : MonoBehaviour
{
    private bool mouseover = false;
    private List<Component> children;
    private ViewHandler viewhandler;
    private string GroupLabel;

    private void Start()
    {
        viewhandler = GameObject.FindGameObjectWithTag("Conductor").GetComponent<ViewHandler>();
        if (gameObject.name == "score -1")
            GroupLabel = "none";
        else
            GroupLabel = gameObject.name + "0";
    }

    private void OnMouseEnter()
    {
        if (gameObject.name == "scoreView")
            return;
        if (children == null)
            children = new List<Component>(gameObject.GetComponentsInChildren<visualTestScript>());

        foreach(Component component in children)
        {
            component.gameObject.GetComponent<MouseOver>().highlightOn();
        }
        updateTooltip();
        mouseover = true;
    }

    private void OnMouseExit()
    {
        if (children == null)
            return;
        foreach (Component component in children)
        {
            component.gameObject.GetComponent<MouseOver>().highlightOff();
        }
        viewhandler.solarTooltip.SetActive(false);
        mouseover = false;
    }

    void Update()
    {
        if (mouseover)
        {
            if (Input.GetMouseButtonDown(0))
                Debug.Log(gameObject.name);
        }
    }

    private void updateTooltip()
    {
        viewhandler.solarTooltip.SetActive(true);
        viewhandler.solarTooltip.transform.position = Input.mousePosition + new Vector3(100f, -150f);

        viewhandler.solarTooltip.GetComponentInChildren<Text>().text = GroupLabel;
    }
}
