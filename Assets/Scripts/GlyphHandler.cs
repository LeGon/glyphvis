﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlyphHandler : MonoBehaviour
{
    private GlyphData glyph;
    public  GameObject conductor;
    public int gid = -1;
    public string state = "Asleep";
    private bool move = false;
    private bool moved = false;
    private float movespeed = 5f;
    private Vector3 targetpos;
    public GameObject atmosphere;
    public GameObject streaming;

    public void wakeUp()
    {
        //this:Hello World!
        state = "Hello world!";
        //this: Oh what's this?
        char[] temp = { 'G', 'I', 'D' };
        gid = int.Parse(gameObject.name.TrimStart(temp));
        conductor = GameObject.FindGameObjectWithTag("Conductor");
        glyph = gogetGlyphData();
        applyGlyphdata();

        //this: What do I look like?

    }

    // Update is called once per frame
    void Update()
    {
        if (move && moved)
            goBack(movespeed,0);
        else if (move)
            goThere(targetpos, movespeed,0);
    }

    private GlyphData gogetGlyphData()
    {
        //this: Hey I found this ID in my Tag, so...
        //conductor: Yepp! Say no more! Here you go:
        GlyphData l_glyph = conductor.GetComponent<ViewHandler>().whatsWithThatID("GID" + gid);
        //this: nice!
        if(l_glyph.id == -1)
        {
            //this: Hey wait a minute. That is not my ID.
            //conductor: It's not? Whats the value saying?
            if (l_glyph.values.ContainsKey(-1))
            {
                state = l_glyph.values[-1];
                //conductor: Well, if that's what it says!
                //this: Ok and now?
                //conductor: ¯\_(ツ)_/¯
            }
        }
        else
        {
            state = "Got ID: " + l_glyph.id;
        }

        return l_glyph;
    }

    public Dictionary<int,string> getValues()
    {
        return glyph.values;
    }

    public List<int> getGlyphSchema()
    {
        JsonLabel schema = conductor.GetComponent<ViewHandler>().schema;
        return schema.glyph;
    }


    public void setPosition(Vector3 newpos)
    {
        this.glyph.position = newpos;
        transform.position = newpos;
    }

    public void goThere(Vector3 newpos, float speed, int publ)
    {
        if (publ == 1)
        {
            targetpos = newpos;
            movespeed = speed;
            move = true;
            publ--;
        }
        transform.position = Vector3.MoveTowards(transform.position, newpos, speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, newpos) == 0f)
        {
            move = false;
            moved = true;
        }
        
    }

    public void goBack(float speed, int publ)
    {
        if(publ == 1)
        {
            movespeed = speed;
            move = true;
            publ--;
        }
        transform.position = Vector3.MoveTowards(transform.position, glyph.position, speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, glyph.position) == 0f)
        {
            move = false;
            moved = false;
        }
            
    }

    private void applyGlyphdata()
    {
        //Debug.Log("applyGlyphData  -----    " + glyph.toString());
        
        atmosphere.GetComponent<atmosphereHandler>().changeColor(float.Parse(glyph.values[4])/10f, float.Parse(glyph.values[5])/100f);
        atmosphere.GetComponent<atmosphereHandler>().changeSize(int.Parse(glyph.values[14]));

        List<int> streams = new List<int>();
        streams.Add(int.Parse(glyph.values[6]));
        streams.Add(int.Parse(glyph.values[7]));
        streams.Add(int.Parse(glyph.values[8]));
        streams.Add(int.Parse(glyph.values[9]));
        streaming.GetComponent<streaming>().changeMoons(streams);
        streaming.GetComponent<streaming>().changeOrientation(int.Parse(glyph.values[3]));
    }

    public void changeShader()
    {
        atmosphere.GetComponent<atmosphereHandler>().changeColor(float.Parse(glyph.values[4]) / 10f, float.Parse(glyph.values[5]) / 100f);
    }

    public void updateTooltip()
    {
        GameObject tooltip = conductor.GetComponent<ViewHandler>().tooltip;

        tooltip.SetActive(true);
        tooltip.GetComponent<Tooltip>().updateTooltip(glyph);

        tooltip.transform.position = Input.mousePosition + new Vector3(100f,-150f);
    }
}
