﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public Text title;
    public Text year;
    public Text age;
    public Text runtime;
    public Text netflix;
    public Text hulu;
    public Text primeV;
    public Text disney;
    public Text genre;
    public Text imdb;
    public Text rottent;

    public void updateTooltip(GlyphData glyphData)
    {
        title.text = glyphData.values[1];
        year.text = glyphData.values[2];
        if (glyphData.values[3] == "-1")
            age.text = "none";
        else
            age.text = glyphData.values[3];
        imdb.text = glyphData.values[4];
        rottent.text = glyphData.values[5];
        if (glyphData.values[6] == "1")
            netflix.color = Color.black;
        else
            netflix.color = Color.gray;
        if (glyphData.values[7] == "1")
            hulu.color = Color.black;
        else
            hulu.color = Color.gray;
        if (glyphData.values[8] == "1")
            primeV.color = Color.black;
        else
            primeV.color = Color.gray;
        if (glyphData.values[9] == "1")
            disney.color = Color.black;
        else
            disney.color = Color.gray;

        genre.text = glyphData.values[11];
        runtime.text = glyphData.values[14] + " min";
    }
}
