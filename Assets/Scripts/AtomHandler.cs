﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomHandler : MonoBehaviour
{
    public GameObject elektron;
    public List<Material> valueColorList;

    private bool gotID;
    private List<GameObject> elektronsList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        gotID = false;
        while (!gotID)
        {
            if(gameObject.GetComponentInParent<GlyphHandler>().gid != -1)
            {
                gotID = true;
            }
        }

        AddEletrons(gameObject.GetComponentInParent<GlyphHandler>().getValues(),
            gameObject.GetComponentInParent<GlyphHandler>().getGlyphSchema());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddEletrons(Dictionary<int,string> values, List<int> glyph)
    {
        int i = 0;
        float winkel = 0;
        foreach (int entry in glyph)
        {
            if (values[entry] == "1") { i++;}
        }
        if(i!=0)
            winkel = 180 / i;
        int e = 0;
        foreach (int entry in glyph)
        {
            e++;
            if (values[entry] == "1") {
                GameObject newelektron = Instantiate(elektron, transform.position, transform.rotation);
                elektronsList.Add(newelektron);
                newelektron.GetComponentInChildren<MeshRenderer>().material = valueColorList[entry%12];
                newelektron.transform.SetParent(transform);
                newelektron.transform.Rotate(new Vector3((winkel-180*(e%2)), 180 * (e % 2), winkel * e));

            }
        }
    }
}
