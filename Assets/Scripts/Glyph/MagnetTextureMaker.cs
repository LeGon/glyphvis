﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MagnetTextureMaker
{
    const int width = 2;
    const int height = 1;

    public static Texture2D Create(Color color1, Color color2, TextureWrapMode textureWrapMode = TextureWrapMode.Clamp, FilterMode filterMode = FilterMode.Point, bool isLinear = false, bool hasMipMap = false)
    {
        if (color1 == null || color2 == null)
        {
            Debug.LogError("Not enough colors assigned");
            return null;
        }

        // build gradient from colors
        var colorKeys = new GradientColorKey[2];
        var alphaKeys = new GradientAlphaKey[2];

       
            colorKeys[0].color = color1;
            colorKeys[0].time = 0f;
            alphaKeys[0].alpha = color1.a;
            alphaKeys[0].time = 0f;

            colorKeys[1].color = color2;
            colorKeys[1].time = 1f;
            alphaKeys[1].alpha = color2.a;
            alphaKeys[1].time = 1f;

        // create gradient
        Gradient gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);

        // create texture
        Texture2D outputTex = new Texture2D(height, width, TextureFormat.ARGB32, false, isLinear);
        outputTex.wrapMode = textureWrapMode;
        outputTex.filterMode = filterMode;

        // draw texture
        for (int i = 0; i < width; i++)
        {
            outputTex.SetPixel(0, i, gradient.Evaluate((float)i / (float)(width-1)));
            //Debug.Log(gradient.Evaluate((float)i / (float)width));
        }
        outputTex.Apply(false);

        return outputTex;
    } // BuildGradientTexture
}
