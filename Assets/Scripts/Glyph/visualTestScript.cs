﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class visualTestScript : MonoBehaviour
{

    // Creates a material from shader and texture references.
    Texture texture;
    public Material shader;
    public Color[] colors = { Color.cyan, Color.blue };
    public bool gradient = false;

    // Start is called before the first frame update
    void Start()
    {
        Material mat = gameObject.GetComponent<MeshRenderer>().material;

        //if (gradient)
        //    texture = GradientTextureMaker.Create(colors);
        //else
        //    texture = MagnetTextureMaker.Create(colors[0], colors[1]);

        //mat.mainTexture = texture;
        //mat.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void createTexture(Color[] colorarray)
    {
        colors = colorarray;

        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();

        if (gradient)
            texture = GradientTextureMaker.Create(colors);
        else
            texture = MagnetTextureMaker.Create(colorarray[0], colorarray[1]);
        shader = new Material(GameObject.FindGameObjectWithTag("Conductor").GetComponent<ViewHandler>().glyphShader);
        meshRenderer.material = new Material(shader.shader);
        meshRenderer.material.mainTexture = texture;
        meshRenderer.material.color = Color.white;
    }

}
