﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoreColor
{

    public static Color GetColor(float score)
    {
        if (score > 1f || score < 0f)
            return Color.gray;

        Color color2 = new Color(102f/255f,204f / 255f, 51f / 255f);
        Color color3 = new Color(255f / 255f, 204f / 255f, 51f / 255f);
        Color color1 = new Color(255f / 255f, 0,0);

        // build gradient from colors
        var colorKeys = new GradientColorKey[3];
        var alphaKeys = new GradientAlphaKey[3];


        colorKeys[0].color = color1;
        colorKeys[0].time = 0f;
        alphaKeys[0].alpha = color1.a;
        alphaKeys[0].time = 0f;

        colorKeys[1].color = color3;
        colorKeys[1].time = 0.5f;
        alphaKeys[1].alpha = color3.a;
        alphaKeys[1].time = 0.5f;

        colorKeys[2].color = color2;
        colorKeys[2].time = 1f;
        alphaKeys[2].alpha = color2.a;
        alphaKeys[2].time = 1f;

        // create gradient
        Gradient gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);

        return gradient.Evaluate(score);
    }

}

