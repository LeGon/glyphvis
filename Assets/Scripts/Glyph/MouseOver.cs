﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOver : MonoBehaviour
{
    public Material highlightMat;
    public Color hlColor = new Color(0f, 115f/255f, 1f, 1f);
    public GameObject planet;
    public GameObject ring;
    public List<GameObject> streaming;

    private Material ringMat;

    void OnMouseEnter()

    {
        gameObject.GetComponentInParent<GlyphHandler>().updateTooltip();

        planet.SetActive(true);
        ring.GetComponent<MeshRenderer>().material.color = hlColor;
        ringMat =  streaming[0].GetComponent<MeshRenderer>().material;
        foreach (GameObject item in streaming)
        {
            item.GetComponent<MeshRenderer>().material = highlightMat;
        }

        //Debug.Log("Hey!");
    }
    void OnMouseExit()
    {
        GameObject.FindGameObjectWithTag("ToolTip").SetActive(false);

        if (ringMat.shader == highlightMat.shader)
        {
            //Debug.Log("OnMouseExit:  ist selected");
            return;
        }

        //Debug.Log("OnMouseExit:  is nicht selected");
        foreach (GameObject item in streaming)
        {
            item.GetComponent<MeshRenderer>().material = ringMat;
        }
        ring.GetComponent<MeshRenderer>().material.color = ringMat.color;
        planet.SetActive(false);
    }

    private void OnMouseDown()
    {
        GameObject tempGo = gameObject.GetComponentInParent<GlyphHandler>().gameObject;
        ViewHandler viewhandler = GameObject.FindGameObjectWithTag("Conductor").GetComponent<ViewHandler>();

        if (viewhandler.selectedGo == tempGo)
        {
            CameraMovement input = GameObject.FindGameObjectWithTag("InputController").GetComponent<CameraMovement>();
            input.target = tempGo.transform;
            input.radius = 5f;
            viewhandler.backButton.gameObject.SetActive(true);
            return;
        }
            

        //Debug.Log("OnMouseDown:  is nicht selected");
        viewhandler.selectGlyph(tempGo, ringMat);
        ringMat = highlightMat;

    }

    public void deselect(Material oldMat)
    {
        foreach (GameObject item in streaming)
        {
            item.GetComponent<MeshRenderer>().material = oldMat;
        }
        ring.GetComponent<MeshRenderer>().material.color = oldMat.color;
        planet.SetActive(false);
    }

    public void highlightOn()
    {
        planet.SetActive(true);
        ring.GetComponent<MeshRenderer>().material.color = hlColor;
        ringMat = streaming[0].GetComponent<MeshRenderer>().material;
        foreach (GameObject item in streaming)
        {
            item.GetComponent<MeshRenderer>().material = highlightMat;
        }
    }

    public void highlightOff()
    {
        if (ringMat.shader == highlightMat.shader)
            return;
        
        foreach (GameObject item in streaming)
        {
            item.GetComponent<MeshRenderer>().material = ringMat;
        }
        ring.GetComponent<MeshRenderer>().material.color = ringMat.color;
        planet.SetActive(false);
    }
}
