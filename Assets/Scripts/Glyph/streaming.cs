﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class streaming : MonoBehaviour
{

    public List<GameObject> services;
    public Transform billboard;

    public void changeMoons(List<int> streams)
    {
        //Debug.Log("--->   changeMoons() \n" + "streams: " + streams.ToString() + "\n count:  " + streams.Count);
        if (streams.Count < 1)
            return;
        int i = 0;

        foreach(GameObject service in services)
        {
            if (streams[i] == 1)
            {
                service.SetActive(true);
            }
            i++;
        }
    }

    public void changeOrientation(int age)
    {
        if(age == 0)
        {
            //Debug.Log(age + ":0   - " + transform.rotation);
            return;
        }
        if (age == -1)
        {
            //Debug.Log(age + ":-1   - " + transform.rotation);
            billboard.Rotate(new Vector3(1, 0, 0), 90f);
        }
        if(age == 18)
        {
            //Debug.Log(age + ":18   - " + transform.rotation);
            billboard.Rotate(new Vector3(0, 0, 1), 90f);
        }
        if (age == 16)
        {
            //Debug.Log(age + ":16   - " + transform.rotation);
            billboard.Rotate(new Vector3(0, 0, 1), 67.5f);
        }
        if (age == 13)
        {
            //Debug.Log(age + ":13   - " + transform.rotation);
            billboard.Rotate(new Vector3(0, 0, 1), 45f);
        }
        if (age == 7)
        {
            //Debug.Log(age + ":7   - " + transform.rotation);
            billboard.Rotate(new Vector3(0, 0, 1), 22.5f);
        }


    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
