﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// returns gradient Texture2D (size=256x1)
// Source: https://github.com/UnityCommunity/UnityLibrary/blob/master/Assets/Scripts/Texture/GradientTextureMaker.cs

public static class GradientTextureMaker
{
    const int width = 256;
    const int height = 1;

    public static Texture2D Create(Color[] colors, TextureWrapMode textureWrapMode = TextureWrapMode.Clamp, FilterMode filterMode = FilterMode.Point, bool isLinear = false, bool hasMipMap = false)
    {
        List<Color> newcolor = new List<Color>();

        if (colors == null || colors.Length == 0)
        {
            Debug.LogError("No colors assigned");
            return null;
        }

        int length = colors.Length;

        if (colors.Length == 2)
        {
            newcolor.Add(colors[0]);
            newcolor.Add(colors[0]);
            newcolor.Add(colors[0]);
            newcolor.Add(colors[0]);
            newcolor.Add(colors[1]);
            newcolor.Add(colors[1]);
            newcolor.Add(colors[1]);
            newcolor.Add(colors[1]);
            length = newcolor.Count;
        }

            
        if (colors.Length > 8)
        {
            Debug.LogWarning("Too many colors! maximum is 8, assigned: " + colors.Length);
            length = 8;
        }



        // build gradient from colors
        var colorKeys = new GradientColorKey[length];
        var alphaKeys = new GradientAlphaKey[length];

        float steps = length - 1f;
        for (int i = 0; i < length; i++)
        {
            float step = i / steps;
            colorKeys[i].time = step;
            alphaKeys[i].time = step;
            if(colors.Length == 2)
            {
                colorKeys[i].color = newcolor[i];
                alphaKeys[i].alpha = newcolor[i].a;
            }
            else
            {
                colorKeys[i].color = colors[i];
                alphaKeys[i].alpha = colors[i].a;
            }
            
               
        }

        // create gradient
        Gradient gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);

        // create texture
        Texture2D outputTex = new Texture2D(height, width, TextureFormat.ARGB32, false, isLinear);
        outputTex.wrapMode = textureWrapMode;
        outputTex.filterMode = filterMode;

        // draw texture
        for (int i = 0; i < width; i++)
        {
            outputTex.SetPixel(0, i, gradient.Evaluate((float)i / (float)width));
        }
        outputTex.Apply(false);

        return outputTex;
    } // BuildGradientTexture

} // class