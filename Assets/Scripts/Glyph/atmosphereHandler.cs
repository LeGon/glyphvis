﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class atmosphereHandler : MonoBehaviour
{

    public GameObject atmoMesh;
    public int blubb;

    public void changeColor(float score1, float score2)
    {
        Color[] colors = { ScoreColor.GetColor(score2), ScoreColor.GetColor(score1) };
        atmoMesh.GetComponent<visualTestScript>().createTexture(colors);
    }

    public void changeSize(int runtime)
    {
        if(runtime< 60)
        {
            atmoMesh.transform.localScale = new Vector3(.5f, .5f, .5f);
            return;
        }
        if (runtime > 120)
        {
            atmoMesh.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            return;
        }
        float y = (1f / 60f) * runtime - .5f;
        atmoMesh.transform.localScale = new Vector3(y, y, y);
    }

}
