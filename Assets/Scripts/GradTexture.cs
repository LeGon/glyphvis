﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradTexture : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<RawImage>().texture = GetTexture();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateTexture()
    {
        gameObject.GetComponent<RawImage>().texture = GetTexture();
    }


    public Texture GetTexture()
    {
        Color color2 = new Color(102f / 255f, 204f / 255f, 51f / 255f);
        Color color3 = new Color(255f / 255f, 204f / 255f, 51f / 255f);
        Color color1 = new Color(255f / 255f, 0, 0);

        // build gradient from colors
        var colorKeys = new GradientColorKey[3];
        var alphaKeys = new GradientAlphaKey[3];


        colorKeys[0].color = color1;
        colorKeys[0].time = 0f;
        alphaKeys[0].alpha = color1.a;
        alphaKeys[0].time = 0f;

        colorKeys[1].color = color3;
        colorKeys[1].time = 0.5f;
        alphaKeys[1].alpha = color3.a;
        alphaKeys[1].time = 0.5f;

        colorKeys[2].color = color2;
        colorKeys[2].time = 1f;
        alphaKeys[2].alpha = color2.a;
        alphaKeys[2].time = 1f;

        // create gradient
        Gradient gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);

        TextureWrapMode textureWrapMode = TextureWrapMode.Clamp;
        FilterMode filterMode = FilterMode.Point;
        bool isLinear = false;
        bool hasMipMap = false;

        // create texture
        Texture2D outputTex = new Texture2D(1, 256, TextureFormat.ARGB32, false, isLinear);
        outputTex.wrapMode = textureWrapMode;
        outputTex.filterMode = filterMode;

        // draw texture
        for (int i = 0; i < 256; i++)
        {
            outputTex.SetPixel(0, i, gradient.Evaluate((float)i / (float)256));
        }
        outputTex.Apply(false);
        return outputTex;
    }

}
