﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;


public class JsonData : MonoBehaviour
{
    string filename = "movielense3d.23.11.2017.schema.json";
    public string path;

    SceneData sceneData;
    SceneData tempData;
    JsonWrapper covData;
    JsonLabel schema = new JsonLabel();

    // Start is called before the first frame update
    void Start()
    {
        path = Application.persistentDataPath + "/" + filename;
        Debug.Log(path);

        
  
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveData();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReadSchema();
            //ReadFeatures();
            ReadPosition();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            DieBack();
        }
    }

    void SaveData()
    {
        /*string filename = "data.json";
        path = Application.persistentDataPath + "/" + filename;
        string contents = JsonUtility.ToJson(list2, true);
        System.IO.File.WriteAllText(path, contents);
        */
    }

    public void ReadData()
    {
        ReadSchema();
        ReadFeatures();
        ReadPosition();
        mergeData();
    }

    public void ReadPosition()
    {
        try
        {
            string filename = "movielense3d.23.11.2017.position.t-sne.json";
            path = Application.persistentDataPath + "/movielense3d/" + filename;
            if (System.IO.File.Exists(path))
            {
                string contents = System.IO.File.ReadAllText(path);
                sceneData = JsonUtility.FromJson<SceneData>(contents);
                //Debug.Log(sceneData.glyphData[0].position.x);
            }
            else
            {
                Debug.Log("Unable to read the date, file does not exist");
                sceneData = new SceneData();
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    public void ReadSchema()
    {
        try
        {
            string filename = "movielense3d.23.11.2017.schema.json";
            path = Application.persistentDataPath + "/movielense3d/" + filename;
            if (System.IO.File.Exists(path))
            {
                string contents = System.IO.File.ReadAllText(path);
                schema = JsonConvert.DeserializeObject<JsonLabel>(contents);

                //Debug.Log("schema");
                string text = "";
                int i = 1;
                foreach (var item in schema.label)
                {
                    text += item.Key + " : " + item.Value + "\n";
                    i++;
                }
                //Debug.Log(text);
            }
            else
            {
                Debug.Log("Unable to read the date, file does not exist");
                schema = new JsonLabel();
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    public void ReadFeatures()
    {
        try
        {
            string filename = "movielense3d.23.11.2017.feature.json";
            path = Application.persistentDataPath + "/movielense3d/" + filename;
            if (System.IO.File.Exists(path))
            {
                string contents = System.IO.File.ReadAllText(path);
                tempData = JsonConvert.DeserializeObject<SceneData>(contents);
                Debug.Log("Feature Item count: " + tempData.featureData.Count);
            }
            else
            {
                Debug.Log("Unable to read the date, file does not exist");
                tempData = new SceneData();
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    public SceneData getGlyphData()
    {
        return sceneData;
    }

    public SceneData mergeData()
    {
        //Debug.Log(">>mergeData");
        try
        {
            if (sceneData != null)
            {
                //Debug.Log("Feature Item count: " + tempData.featureData.Count);
                foreach (FeatureData feature in tempData.featureData)
                {
                    foreach (GlyphData glyph in sceneData.glyphData)
                    {
                        if (feature.id == glyph.id)
                        {
                            glyph.values = new Dictionary<int, string>(feature.values);
                            break;
                        }
                    }
                }
                
                    //string text = "";
                    //text += "id: " + sceneData.glyphData[0].id + "\n";
                    //text += "Position: " + sceneData.glyphData[0].position.ToString() + "\n";
                    //foreach (KeyValuePair<int,string> item in sceneData.glyphData[0].values)
                    //{
                    //    text += item.Key + " : " + item.Value + "\n";
                    //}
                    //Debug.Log(text);
                

                if (schema != null)
                {
                    sceneData.schema = schema;
                    //Debug.Log("label.count: " + sceneData.schema.label.Count);
                }
                //Debug.Log("<<mergeData");
                return sceneData;
            }
            else
            {
                Debug.Log("Unable to read the date. Date does not exist");
                sceneData = new SceneData();
            } 
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
            sceneData = new SceneData();
        }
        return sceneData;
    }

    private void DieBack()
    {
        JsonLabel schema = new JsonLabel();
        //Dictionary<int, string> label = new Dictionary<int, string>();
        schema.glyph = new List<int>()
        { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
        schema.color = new List<int>()
        { 5};
        schema.tooltip = new List<int>()
        {1,2,3,4 };

        schema.label.Add(1, "movie id");
        schema.label.Add(2, "movie title");
        schema.label.Add(3, "movie release");
        schema.label.Add(4, "imdb url");
        schema.label.Add(5, "Action");
        schema.label.Add(6, "Adventure");


       


        string filename = "data.json";
        path = Application.persistentDataPath + "/" + filename;

        string jsonStr = JsonConvert.SerializeObject(schema,Formatting.Indented);
        Debug.Log("serialized text = " + jsonStr);

        File.WriteAllText(path, jsonStr);

        Debug.Log("DieBack beendet");
    }

    public void loadCoronaData()
    {
        try
        {
            filename = "o_time-series-19-covid-combined_json.json";
            path = Application.persistentDataPath + "/Corona/" + filename;
            if (System.IO.File.Exists(path))
            {
                string contents = System.IO.File.ReadAllText(path);
                covData = JsonUtility.FromJson<JsonWrapper>(contents);
                Debug.Log("covList: " + covData.covList.Count);
                Debug.Log("first item: " + covData.covList[0].toString());
                   
            }
            else
            {
                Debug.Log("Unable to read the date, file does not exist");
                covData = new JsonWrapper();
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    private SceneData mergeCoronaData(JsonWrapper covData)
    {
        Debug.Log("### mergeCoronaData ###");
        Debug.Log("covData: " + covData.covList.Count);
        int i =0 ,j = 0;
        Dictionary<int, string> values;
        SceneData scene = new SceneData();
        Debug.Log(scene.toString());
        List<GlyphData> glyphdata = new List<GlyphData>();
        foreach(TimeSeriesCov item in covData.covList)
        {
            GlyphData glyph = new GlyphData();
            glyph.id = i++;
            j = 0;
            values = new Dictionary<int, string>();
            foreach(string value in item.toStringList())
            {
                values.Add(j++, value);
            }
            glyph.values = values;
            glyph.position = new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), Random.Range(-50, 50));
            glyphdata.Add(glyph);
        }

        scene.glyphData = glyphdata;
        scene.schema = new JsonLabel();

        Debug.Log(scene.toString());
        Debug.Log("___ mergeCoronaData ___");
        return scene;
    }

    public void ReadData2()
    {
        Debug.Log("### ReadData2 ###");
        loadCoronaData();

        JsonWrapper var1 = covData;
        Debug.Log(var1.covList.Count);

        sceneData = mergeCoronaData(var1);
        Debug.Log(sceneData.glyphData[2].toString());
        Debug.Log(sceneData.toString());
        Debug.Log("___ ReadData2 ___");
    }
}