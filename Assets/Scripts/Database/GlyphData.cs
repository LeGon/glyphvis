﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GlyphData
{
    public int id;
    public Vector3 position;
    public Dictionary<int, string> values;
    public GameObject glyphGO;

    public string toString()
    {
        string text = "id: " + id
            + "\n position: " + position.ToString()
            + "\n values:";
        foreach(KeyValuePair<int, string> item in values)
        {
            text += item.Key + " : " + item.Value + "\n";
        }

        return text;
    }
}
