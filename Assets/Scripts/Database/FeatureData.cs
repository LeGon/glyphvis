﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FeatureData
{
    public Dictionary<int, int> features = new Dictionary<int, int>();
    public int id;
    public Dictionary<int, string> values = new Dictionary<int, string>();
}
