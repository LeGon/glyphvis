﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Igor
{
    //Datensammlerklasse als Vorstufe zur Formatierung
    public int id;
    public string title;
    public int year;
    public string age;
    public string IMDb;
    public string RottenT;
    public int netflix;
    public int hulu;
    public int primev;
    public int disney;
    public int type;
    public string directors;
    public string genres;
    public string country;
    public string language;
    public int runtime;
}
