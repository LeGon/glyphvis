﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class moviLib
{
    public int id;
    public string title;
    public int year;
    public int age;
    public float IMDb = -1.0f;
    public float RottenT = -1.0f;
    public int netflix;
    public int hulu;
    public int primev;
    public int disney;
    public int type;
    public string directors;
    public string genres;
    public string country;
    public string language;
    public int runtime;

    public string toString()
    {
        string text = "id= " + id
            + " : " + title
            + "\nYear: " + year
            + "\nAge: " + age
            + "\nIMDb: " + IMDb
            + "\nrottenT: " + RottenT
            + "\nNetflix: " + netflix
            + "\nHulu: " + hulu
            + "\nPrimeVideo: " + primev
            + "\nDisney+ " + disney
            + "\n";

        //---> #quickString
        text += "Directors: " + directors
            + "\nGenre:" + genres
            + "\nCountry:" + country
            + "\nLaguage:" + language;

        return text;

        //<--- #quickString

        /*---> when string[]
        if (directors != null){
            text += "Directors: ";
            foreach (string person in directors)
            {
                text += person + ", ";
            }
            text += "\n";
        }
        if (genres != null)
        {
            text += "Genres: ";
            foreach (string genre in genres)
            {
                text += genre + ", ";
            }
            text += "\n";
        }
        if (country != null)
        {
            text += "Countries: ";
            foreach (string item in country)
            {
                text += item + ", ";
            }
            text += "\n";
        }
        if (language != null)
        {
            text += "Languages: ";
            foreach (string lang in language)
            {
                text += lang + ", ";
            }
            text += "\n";
        }

        text += "runtime: " + runtime
            + "\n-------- ";

        return text;
        ---> when string[] */
    }
}
