﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SceneData
{
    public List<GlyphData> glyphData = new List<GlyphData>();
    public List<FeatureData> featureData = new List<FeatureData>();
    public JsonLabel schema = new JsonLabel();

    public string toString()
    {
        string gd, fd, sd;

        if (glyphData == null)
            gd = "null";
        else
            gd = glyphData.Count.ToString();

        if (featureData == null)
            fd = "null";
        else
            fd = featureData.Count.ToString();

        if (schema == null)
            sd = "null";
        else
            sd = schema.tooltip.Count.ToString();

        return "sceneData: glyphData=" + gd + " featureData=" + fd + " schema=" + sd;
    }

    public GlyphData GetGlyphByID(int id)
    {
        GlyphData thisglyph = new GlyphData();

        foreach (GlyphData glyph in glyphData)
        {
            if(glyph.id == id)
            {
                thisglyph = glyph;
                break;
            }
        }
        return thisglyph;
    }
}
