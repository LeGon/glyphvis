﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TimeSeriesCov
{
    public int confirmed;
    public string country;
    public string date;
    public int deaths;
    public float lat;
    public float lon;
    public string province;
    public int recovered;

    public string toString()
    {
        return confirmed 
            + ", " + country
            + ", " + date
            + ", " + deaths
            + ", " + lat
            + ", " + lon
            + ", " + province
            + ", " + recovered;
    }

    public List<string> toStringList()
    {
        List<string> list = new List<string>();
        list.Add(confirmed.ToString());
        list.Add(country);
        list.Add(date);
        list.Add(deaths.ToString());
        list.Add(lat.ToString());
        list.Add(lon.ToString());
        list.Add(province);
        list.Add(recovered.ToString());

        return list;

    }
}
