﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class csvData : MonoBehaviour
{
    public string path;
    string[] lines;
    string tmpfilename = "MOD_mix250.csv";
    public List<moviLib> movielib;

    // Start is called before the first frame update
    void Start()    {  }

    // Update is called once per frame
    void Update()  {  }

    public Igor[] ReadCsvFile(string filename)
    {
        path = Application.persistentDataPath + "/data/" + filename;
        string fileData = System.IO.File.ReadAllText(path);
        //lines  = fileData.Split('\n');
        Igor[] igorlist = CSVSerializer.Deserialize<Igor>(fileData);
        return igorlist;
    }

    public void selectFile(Dropdown drpdwn)
    {
        tmpfilename = drpdwn.options[drpdwn.value].text;
    }

    public void getMovieODdata()
    {
        //tmpfilename = "Assets/Resources/MOD_mix250.csv";
        //string file = Resources.Load<string>("")
        Igor[] igorlist = ReadCsvFile(tmpfilename);
        movielib = new List<moviLib>();
        foreach(Igor igor in igorlist)
        {
            moviLib movie = new moviLib();
            movie.id = igor.id;
            movie.title = igor.title;
            movie.year = igor.year;

            if (igor.age == null)
                movie.age = -1;
            else if (Regex.Replace(igor.age, "[^0-9]", "") == "")
                movie.age = 0;
            else
                movie.age = int.Parse(Regex.Replace(igor.age, "[^0-9]", ""));

            if (igor.IMDb != null)
                movie.IMDb = float.Parse(igor.IMDb.Replace(".",","));
            if(igor.RottenT != null)
                movie.RottenT = float.Parse(igor.RottenT.Replace("%", ""));

            movie.netflix = igor.netflix;
            movie.hulu = igor.hulu;
            movie.primev = igor.primev;
            movie.disney = igor.disney;
            movie.type = igor.type;
            if (igor.directors != null)
                movie.directors = igor.directors;  //directors.Split(","[0]);   #quickString
            if (igor.genres != null)
                movie.genres = igor.genres;   //genres.Trim().Split(","[0]);   #quickString
            if (igor.country != null)
                movie.country = igor.country;   //country.Trim().Split(","[0]);   #quickString
            if (igor.language != null)
                movie.language = igor.language;   //language.Trim().Split(","[0]);   #quickString
            movie.runtime = igor.runtime;

            movielib.Add(movie);
        }
        Debug.Log(movielib.Count);
        Debug.Log(movielib[0].toString());
        //Debug.Log(movielib[18].toString());
        //Debug.Log(movielib[2552].toString());

        //checkforgenres();
        //checkforstuff();

        return;
    }

    public void checkforgenres()
    {
        List<string> genrelist = new List<string>();
        List<int> genrecount = new List<int>();
        string[] longestgenre = { "1", "2" };
        List<string> titleList = new List<string>();
        List<int> titlecount = new List<int>();

        foreach (moviLib movie in movielib)
        {
            // angepasst wegen #quickString
            if (movie.genres!= null)
            {
                string[] genres = movie.genres.Trim().Split(","[0]);
                if(movie.genres.Length > longestgenre.Length)
                    longestgenre = genres;
                foreach (string genre in genres)
                {
                    if (!genrelist.Contains(genre))
                    {
                        genrelist.Add(genre);
                        genrecount.Add(1);
                    }
                    else
                        genrecount[genrelist.IndexOf(genre)]++;
                }
            }

            if (!titleList.Contains(movie.title))
            {
                titleList.Add(movie.title);
                titlecount.Add(1);
            }
            else
                titlecount[titleList.IndexOf(movie.title)]++;
        }

        string text = "";
        for (int i = 0; i<genrelist.Count; i++)
        {
            text += genrelist[i] + " : " + genrecount[i] + "\n";
        }
        Debug.Log(text);
        Debug.Log("meiste genres pro film: " + longestgenre.Length);
        text = "";
        for (int i = 0; i < titlecount.Count; i++)
        {
            if(titlecount[i] > 1)
                text += titleList[i] + " : " + titlecount[i] + "\n";
        }
        Debug.Log("Anzahl Titel: " + titleList.Count);
        Debug.Log(text);
    }

    public void checkforstuff()
    {
        //datenauswertung für string[] variablen
        List<string> stufflist = new List<string>();
        List<int> stuffcount = new List<int>();
        string[] longeststuff = { "1", "2" };
        string longestStuffMovie = "";
        int mostStuffMovie = 0;
        foreach (moviLib movie in movielib)
        {
            //einfach überall movie.x austauschen

            if (movie.language != null)
            {
                //angepasst egen #quickString
                string[] language = movie.language.Split(","[0]); 
                if (language.Length > longeststuff.Length)
                {
                    longeststuff = language;
                    longestStuffMovie = movie.title;
                }
                foreach (string stuff in language)
                {
                    if (!stufflist.Contains(stuff))
                    {
                        stufflist.Add(stuff);
                        stuffcount.Add(1);
                    }
                    else
                        stuffcount[stufflist.IndexOf(stuff)]++;
                }
            }
        }

        string text = "";
        for (int i = 0; i < stufflist.Count; i++)
        {
            text += stufflist[i] + " : " + stuffcount[i] + "\n";
        }
        Debug.Log("Anzahl Stuff: " + stufflist.Count);
        Debug.Log(text);
        Debug.Log("meiste stuff pro film: " + longeststuff.Length + " im Film " + longestStuffMovie);

        int highest = 0;
        foreach(int stuff in stuffcount)
        {
            if (stuff > highest)
            {
                mostStuffMovie = stuffcount.IndexOf(stuff);
                highest = stuff;
            }
        }
        Debug.Log("meiste stuff: " + highest + " ( " + stufflist[mostStuffMovie] + " )");
    }

    public SceneData getGlyphData()
    {
        List<moviLib> movies = movielib;
        SceneData scene = new SceneData();
        List<GlyphData> glyphdata = new List<GlyphData>();
        Debug.Log("getglyphdata ---->");
        foreach (moviLib movie in movies)
        {
            GlyphData glyph = new GlyphData();
            glyph.id = movie.id;
            int j = 1;
            Dictionary<int, string> values = new Dictionary<int, string>();
            // Values[1]
            values.Add(j++, movie.title);
            // Values[2]
            values.Add(j++, movie.year.ToString());
            // Values[3]
            values.Add(j++, movie.age.ToString());
            // Values[4]
            if (movie.IMDb != -1.0f)
                values.Add(j++, movie.IMDb.ToString());
            else
                values.Add(j++, "-1");
            // Values[5]
            if (movie.RottenT != -1.0f)
                values.Add(j++, movie.RottenT.ToString());
            else
                values.Add(j++, "-1");
            // Values[6]
            values.Add(j++, movie.netflix.ToString());
            // Values[7]
            values.Add(j++, movie.hulu.ToString());
            // Values[8]
            values.Add(j++, movie.primev.ToString());
            // Values[9]
            values.Add(j++, movie.disney.ToString());
            // Values[10]
            values.Add(j++, movie.directors);
            // Values[11]
            values.Add(j++, movie.genres);
            // Values[12]
            values.Add(j++, movie.country);
            // Values[13]
            values.Add(j++, movie.language);
            // Values[14]
            values.Add(j++, movie.runtime.ToString());

            /*if (movie.genres != null)
            {
                foreach (string genre in movie.genres)
                {
                    values.Add(j++, genre);
                }
            }
            else
            {
                values.Add(j++, "no genre");
            }*/

            glyph.values = values;

                
            float score = 0.0f;
            
            if (movie.IMDb != -1f)
            {
                if (movie.RottenT != -1f)
                    score = (movie.IMDb / 10.0f + movie.RottenT / 100.0f) / 2.0f;
                else
                    score = movie.IMDb / 10.0f;
            }
            else
            {
                if (movie.RottenT != -1f)
                    score = movie.RottenT / 100.0f;
                else
                    score = -1.0f;
            }

            float radius = Random.Range(1, 50);
            Vector3 pos = new Vector3(radius, radius, 0);
                while( pos.magnitude > radius)
            {
                pos = new Vector3(Random.Range(-radius, radius), Random.Range(-radius, radius), Random.Range(-radius, radius));
            }
            glyph.position = pos;
                


            //float radius = Random.Range(-200,200);
            //float x = Random.Range(-radius, radius);
            //float z = Mathf.Sqrt((radius * radius) - (x * x))*(-1*(glyph.id%2));
            //float q = new Vector2(x, z).sqrMagnitude;
            //float y = Mathf.Sqrt((radius * radius) - q) * (-1 * (Random.Range(0,9) % 2));
            //if (score == -1.0f)
            //{
            //    y = Random.Range(-20f, -50f);
            //    x = 0f;
            //    z = 0f;
            //}
            //glyph.position = new Vector3(x, y, z);

            glyphdata.Add(glyph);
        }

        scene.glyphData = glyphdata;
        scene.schema = new JsonLabel();

        Debug.Log("getglyphdata <----");
        return scene;
    }
}
