﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JsonLabel
{
    public Dictionary<int, string> label = new Dictionary<int, string>();
    public List<int> glyph = new List<int>();
    public List<int> color = new List<int>();
    public List<int> tooltip = new List<int>();
}
