﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    public Transform camTransform;
    public bool active = true;

    Quaternion originalRotation;

    // Start is called before the first frame update
    void Start()
    {
        camTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
        originalRotation = transform.rotation;
        transform.forward.Set(0f, 1f, 0f);
        active = false;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = camTransform.rotation * originalRotation;
    }

    void LateUpdate()
    {
        if (!active)
        {
            transform.rotation = originalRotation;
            return;
        }

        //transform.LookAt(transform.position + camTransform.rotation * Vector3.forward,
        //    camTransform.transform.rotation * Vector3.up);
        transform.LookAt(camTransform);
        transform.rotation *= originalRotation;
        
    }
}
