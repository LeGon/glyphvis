﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ViewHandler : MonoBehaviour
{
    private Toggle activeView;
    public GameObject selectedGo;
    private Material selectedOldMat;

    public GameObject glyphPreset;
    public GameObject Dataholder;
    public JsonLabel schema;

    public Button backButton;

    public ToggleGroup ViewToggle;
    public Toggle ShaderToggle_Sprite;
    public Toggle ShaderToggle_Emission;
    public Toggle ShaderToggle_Outline;
    public Material glyphShader;
    public ToggleGroup SkyboxToggle;

    public GameObject LegendBestScore;
    public GameObject LegendworstScore;

    public GameObject tooltip;
    public GameObject solarTooltip;

    private SceneData sceneData;
    private View overview = new View();

    // Start is called before the first frame update
    void Start()
    {
        activeView = ViewToggle.ActiveToggles().FirstOrDefault();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void generateGlyphObjects(){
        if (Dataholder)
        {
            sceneData = Dataholder.GetComponent<csvData>().getGlyphData();
            updateSceneInstructions();
            foreach (GlyphData glyph in sceneData.glyphData)
            {
               
                    glyph.glyphGO = Instantiate(glyphPreset, glyph.position, transform.rotation);
                    glyph.glyphGO.name = "GID" + glyph.id;
                    glyph.glyphGO.GetComponent<GlyphHandler>().wakeUp();
                
            }
            Debug.Log(" items skipped");

            ////////////  Für die Covid Daten
            //int i = 0;
            //sceneData = Dataholder.GetComponent<JsonData>().getGlyphData();
            //updateSceneInstructions();
            //foreach (GlyphData glyph in sceneData.glyphData) 
            //{
            //    if (glyph.values[2] == "2020-05-07")
            //    {
            //        glyph.glyphGO = Instantiate(glyphPreset, glyph.position, transform.rotation);
            //        glyph.glyphGO.name = "GID" + glyph.id;
            //        glyph.glyphGO.GetComponent<GlyphHandler>().wakeUp();
            //    }
            //    else
            //        i++;
            //}
            //Debug.Log(i + " items skipped");
        }
        else
            Debug.Log("Dataholder is empty");

    }

    public GlyphData whatsWithThatID(string oid)
    {
        foreach(GlyphData glyph in sceneData.glyphData)
        {
            if("GID" + glyph.id == oid)
            {
                return glyph;
            }
        }
        var err = new GlyphData();
        err.id = -1;
        err.values.Add(-1, "Didn't find ID");
        return err;
    }

    private void updateSceneInstructions()
    {
        schema = sceneData.schema;
    }

    public void basicScoreView()
    {
        // Am besten ein eigenes View-Objekt aus einer View-Klasse erstellen
        View scoreView = new View("scoreView");
        scoreView.setPosition(Vector3.zero);
        overview.AddView(scoreView);

        Dictionary<int, Dictionary<int, float>> solar = new Dictionary<int, Dictionary<int, float>>();
        for (int i = -1; i < 11; i++)
        {
            solar.Add(i, new Dictionary<int, float>());
            scoreView.AddView(new View("score " + i.ToString()));
        }
        float score = 0.0f;

        //Score für jede Glyphe ausrechnen und in ein dictionary schreiben
        Debug.Log("scenedata.count: " + sceneData.glyphData.Capacity);
        foreach (GlyphData glyph in sceneData.glyphData)
        {
            if (glyph.values[4] != "-1")
            {
                if (glyph.values[5] != "-1")
                {
                    score = (float.Parse(glyph.values[4]) + float.Parse(glyph.values[5]) / 10.0f) / 2.0f;
                    solar[(int)score].Add(glyph.id, score);
                }
                else
                {
                    score = float.Parse(glyph.values[4]);
                    solar[(int)score].Add(glyph.id, score);
                }
            }
            else
            {
                if (glyph.values[5] != "-1")
                {
                    score = float.Parse(glyph.values[5]) / 10.0f;
                    solar[(int)score].Add(glyph.id, score);
                }
                else
                {
                    score = -1.0f;
                    solar[-1].Add(glyph.id, score);
                }
            }
            glyph.glyphGO.GetComponent<GlyphHandler>().changeShader();
        }

        //Position der Views aktualisieren und auf die GlyphObjects übertragen
        scoreView.solar(10);
        int j = -1;
        foreach (View view in scoreView.getViewList())
        {
            Debug.Log("i = " + j + "     ||  " + view.toString());
            view.planet(solar[j++]);
            foreach (KeyValuePair<int, Vector3> item in view.getGlyphPos())
            {
                sceneData.GetGlyphByID(item.Key).glyphGO.GetComponent<GlyphHandler>().setPosition(item.Value);
                sceneData.GetGlyphByID(item.Key).glyphGO.transform.SetParent(view.containerGO.transform);
                //sceneData.GetGlyphByID(item.Key).glyphGO.GetComponent<GlyphHandler>().goThere(view.containerGO.transform.position, 5, 1);
            }
        }

        CameraMovement input = GameObject.FindGameObjectWithTag("InputController").GetComponent<CameraMovement>();
        input.radius = scoreView.maxRadius;
        input.scrollscale = 1f;
        input.target = GameObject.FindGameObjectWithTag("Center").transform;
    }

    public void randomView()
    {
        Debug.Log("---> RandomView <---");
        float maxRadius = 50f;
        foreach (GlyphData glyph in sceneData.glyphData)
        {
            float radius = Random.Range(3, maxRadius);
            Vector3 pos = new Vector3(radius, radius, 0);
            while (pos.magnitude >= radius)
            {
                pos = new Vector3(Random.Range(-radius, radius), Random.Range(-radius, radius), Random.Range(-radius, radius));
            }
            glyph.glyphGO.transform.SetParent(null);
            glyph.glyphGO.transform.position = pos;
            glyph.glyphGO.GetComponent<GlyphHandler>().changeShader();
        }
        //scoreView GameObjects zerstören (besser inaktiv setzen)
        Destroy(GameObject.Find("scoreView"));
        for (int i = -1; i < 11; i++)
        {
            Destroy(GameObject.Find("score " + i));
        }

        CameraMovement input = GameObject.FindGameObjectWithTag("InputController").GetComponent<CameraMovement>();
        input.radius = maxRadius;
        input.scrollscale = 0.5f;
        input.target = GameObject.FindGameObjectWithTag("Center").transform;
    }

    public void kugelView()
    {
        Debug.Log("---> KugelView <---");
        int valueIndex = 0;
        float stepsize = 1f;
        int stepcount = 1;
        List<float> steps = new List<float>();
        if (true) {
            //age
            valueIndex = 3;
            stepsize = 5f;
            stepcount = 6;
            steps.AddRange(new float[]{ 0f,7f,13f,16f,18f,-1f});
        }

        int j = 0;
        float maxRadius = stepsize * stepcount;
        foreach (GlyphData glyph in sceneData.glyphData)
        {
            float radius = steps.IndexOf(float.Parse(glyph.values[valueIndex])) * stepsize +5f;
            Vector3 pos = new Vector3(radius, radius, 0);

            float y = Random.Range(-radius, radius);
            float f = Mathf.Sqrt((radius * radius) - (y * y));
            float z = Random.Range(-f, f);
            float dist = new Vector2(y, z).magnitude;
            float x = Mathf.Sqrt((radius * radius) - (dist * dist)) * Mathf.Pow(-1f, (j++ % 2));

            pos = new Vector3(x, y, z);

            glyph.glyphGO.transform.SetParent(null);
            glyph.glyphGO.transform.position = pos;
            glyph.glyphGO.GetComponent<GlyphHandler>().changeShader();
        }
        Destroy(GameObject.Find("scoreView"));
        for (int i = -1; i < 11; i++)
        {
            Destroy(GameObject.Find("score " + i));
        }
        CameraMovement input = GameObject.FindGameObjectWithTag("InputController").GetComponent<CameraMovement>();
        input.radius = maxRadius;
        input.scrollscale = .5f;
        input.target = GameObject.FindGameObjectWithTag("Center").transform;
    }

    public void quickload()
    {
        if(sceneData!= null)
        {
            foreach(GlyphData item in sceneData.glyphData)
            {
                GameObject.Destroy(item.glyphGO);
            }
            sceneData = null;
        }
        Dataholder.GetComponent<csvData>().getMovieODdata();
        generateGlyphObjects();
        applySettings();
    }

    private void changeDesign()
    {
        LegendBestScore.GetComponent<visualTestScript>().createTexture(new Color[] { ScoreColor.GetColor(1f), ScoreColor.GetColor(1f) });
        LegendworstScore.GetComponent<visualTestScript>().createTexture(new Color[] { ScoreColor.GetColor(0f), ScoreColor.GetColor(0f) });
        if (sceneData != null)
        {
            foreach (GlyphData glyph in sceneData.glyphData)
            {
                glyph.glyphGO.GetComponent<GlyphHandler>().changeShader();
            }
        }
    }

    public void applySettings()
    {
        if (ShaderToggle_Sprite.isOn)
            glyphShader = new Material(ShaderToggle_Sprite.GetComponent<MeshRenderer>().material);
        if (ShaderToggle_Emission.isOn)
            glyphShader = ShaderToggle_Emission.GetComponent<MeshRenderer>().material;
        if (ShaderToggle_Outline.isOn)
            glyphShader = new Material(ShaderToggle_Outline.GetComponent<MeshRenderer>().material);

        if (SkyboxToggle.IsActive())
        {
            if (Camera.main.GetComponent<Skybox>().material == SkyboxToggle.ActiveToggles().FirstOrDefault().GetComponent<Skybox>().material)
                Debug.Log("skybox didn't change");
            else
            {
                Camera.main.GetComponent<Skybox>().material = SkyboxToggle.ActiveToggles().FirstOrDefault().GetComponent<Skybox>().material;
            }
        }

        if (activeView == ViewToggle.ActiveToggles().FirstOrDefault())
        {
            Debug.Log("viewselection didn't change");
            changeDesign();
        }
        else
        {
            if (ViewToggle.ActiveToggles().FirstOrDefault().name == "View:Random")
            {
                Debug.Log("randomView selected");
                randomView();
                activeView = ViewToggle.ActiveToggles().FirstOrDefault();
            }

            if (ViewToggle.ActiveToggles().FirstOrDefault().name == "View:Kugel")
            {
                Debug.Log("kugelView selected");
                kugelView();
                activeView = ViewToggle.ActiveToggles().FirstOrDefault();
            }

            if (ViewToggle.ActiveToggles().FirstOrDefault().name == "View:Solar")
            {
                Debug.Log("solarview selected");
                basicScoreView();
                activeView = ViewToggle.ActiveToggles().FirstOrDefault();
            }
            changeDesign();

        }

       


        return;
    }

    public void selectGlyph(GameObject go, Material oldMat)
    {
        if(selectedGo != null)
            selectedGo.GetComponentInChildren<MouseOver>().deselect(selectedOldMat);
        selectedOldMat = oldMat;
        selectedGo = go;
    }

    public void viewback()
    {
        selectedGo = null;
        CameraMovement input = GameObject.FindGameObjectWithTag("InputController").GetComponent<CameraMovement>();
        input.target = GameObject.FindGameObjectWithTag("Center").transform;
        input.radius = 20f;
        return;
    }

    public void setBillboard(bool active)
    {
        if (sceneData != null)
        {
            foreach (GlyphData glyph in sceneData.glyphData)
            {
                glyph.glyphGO.GetComponentInChildren<Billboard>().active = active;
            }
        }
    }
}
